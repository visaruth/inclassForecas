library(e1071)
library(Metrics)
simpletoy <- read.csv("simpletoy.csv", header = TRUE)
plot(simpletoy, pch = 16)

model <- svm(y ~ x, data = simpletoy, kernel='linear')

crossprod(model$coefs,model$SV)

b <- model$rho

Ypre <- predict(model,simpletoy)

points(simpletoy$x,Ypre,col = "red" , pch = 16)

model <- svm(y ~ x, data = simpletoy, kernel='polynomial')

Ypre <- predict(model,simpletoy)

points(simpletoy$x,Ypre,col = "green" , pch = 16)

model <- svm(y ~ x, data = simpletoy, kernel='radial')

Ypre <- predict(model,simpletoy)

points(simpletoy$x,Ypre,col = "pink" , pch = 16)

model <- svm(y ~ x, data = simpletoy, kernel='sigmoid')

Ypre <- predict(model,simpletoy)

points(simpletoy$x,Ypre,col = "yellow" , pch = 16)

obj <- tune(svm, y ~ x, data = simpletoy,kernel='linear' ,ranges = list(epsilon= seq(0.0,1.0,0.1), cost = (1:100)))

bmodel =obj$best.model

xpre <- predict(bmodel,simpletoy)

points(simpletoy$x,xpre,col = "blue" , pch = 16)

obj <- tune(svm, y ~ x, data = simpletoy,kernel='polynomial' ,ranges = list(epsilon= seq(0.0,1.0,0.1), cost = (1:100)))

bmodel =obj$best.model

xpre <- predict(bmodel,simpletoy)

points(simpletoy$x,xpre,col = "magenta" , pch = 16)

obj <- tune(svm, y ~ x, data = simpletoy,kernel='radial' ,ranges = list(epsilon= seq(0.0,1.0,0.1), cost = (1:100)))

bmodel =obj$best.model

xpre <- predict(bmodel,simpletoy)

points(simpletoy$x,xpre,col = "purple" , pch = 16)

obj <- tune(svm, y ~ x, data = simpletoy,kernel='sigmoid' ,ranges = list(epsilon= seq(0.0,1.0,0.1), cost = (1:100)))

bmodel =obj$best.model

xpre <- predict(bmodel,simpletoy)

points(simpletoy$x,xpre,col = "gray" , pch = 16)
