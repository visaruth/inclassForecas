library(e1071)
library(Metrics)
library(mlbench)

data(Ozone)
summary(Ozone)
dim(Ozone)
nrow(Ozone)
ncol(Ozone)
head(Ozone)
names(Ozone)
str(Ozone)
levels(Ozone$V1)
levels(Ozone$V2)
levels(Ozone$V3)

set.seed( 142 )

partitionData <- function( data, fractionOfDataForTrainingData = 0.7 )
{
  numberOfRows <- nrow( data)
  randomRows   <- runif( numberOfRows )
  flag         <- randomRows <= fractionOfDataForTrainingData
  trainingData <- data[ flag, ]
  testingData  <- data[ !flag, ]
  dataSetSplit <- list( trainingData = trainingData, testingData = testingData )
  dataSetSplit
}
par <- partitionData(Ozone)
nrow(par$trainingData)
nrow(par$testingData)
trainset <- na.omit(par$trainingData)
testset <- na.omit(par$testingData)
summary(trainset)
summary(testset)
svm.model <- svm(V4 ~ ., data = trainset, kernel='linear')
svm.W <- t(svm.model$coefs) %*% svm.model$SV
svm.b = svm.model$rho
svm.pred <- predict(svm.model,testset[,targetAttr])
svm.mse <- crossprod(svm.pred - testset[,targetAttr])/length(testset)